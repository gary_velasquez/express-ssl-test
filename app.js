var fs = require('fs'),
  http = require('http'),
  https = require('https'),
  express = require('express');

const proxy = require('http-proxy-middleware');
const {routes} = require('./config/reverseProxy.json');

var port = 5330;

var options2 = {
  key: fs.readFileSync('./cert/cer.key'),
  cert: fs.readFileSync('./cert/bd13b1fddc067e74.crt')
};

var app = express();


/*
var server = https.createServer(options, app).listen(port, function() {
  console.log("Express server listening on port " + port);
});
*/


var server = app.listen(port, function() {
  console.log('beemining-query-service listening on port ' + port);
});


app.get('/api', function(req, res) {
  res.status(200).send('API works.');
});

for (route of routes) {
  const rewrite = {};
  rewrite[route.request] = route.location;
  const options = {
    target: route.host,
    changeOrigin: true,
    ws: true,
    pathRewrite: rewrite,
    secure: true,
    ssl: options2
  }
  app.use(route.request, proxy(options))
}
